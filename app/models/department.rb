class Department < ActiveRecord::Base
  attr_accessible :name, :businessunit_id 
  belongs_to :businessunits
end
